package be.kdg.colors;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import be.kdg.colors.view.MainMenuPresenter;
import be.kdg.colors.view.MainMenuView;

public class MainMenu extends Application {
    @Override
    public void start(Stage stage)  {
        MainMenuView view = new MainMenuView();
        MainMenuPresenter presenter = new MainMenuPresenter(view);
        Scene scene = new Scene(view);
        stage.setScene(scene);
        stage.setTitle("Examples Drawing");
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
