package be.kdg.colors.view;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.BorderPane;


public class Painting1 extends BorderPane {
    private final double DRAWING_WIDTH=500;
    private final double DRAWING_HEIGHT=500;
    private Canvas canvas;

    public Painting1() {
        this.initialiseNodes();
        this.layoutNodes();
    }
    private void initialiseNodes() {
        canvas = new Canvas( DRAWING_WIDTH,  DRAWING_HEIGHT);
    }
    private void layoutNodes() {
        GraphicsContext gc
          = canvas.getGraphicsContext2D();
        gc.strokeLine(10,10,490,490);
        gc.strokeOval(100,100,50,50);
       // gc.fillOval(200,200,180,80);
        gc.fillOval((DRAWING_WIDTH-180)/2,(DRAWING_HEIGHT-80)/2,180,80);
        gc.fillRect(10,300,100,100);
        this.setCenter(canvas);
    }
}