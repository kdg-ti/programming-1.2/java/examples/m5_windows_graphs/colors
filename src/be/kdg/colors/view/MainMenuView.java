package be.kdg.colors.view;

import be.kdg.colors.view.FillPolygon;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import java.util.*;

public class MainMenuView extends VBox {
   private final List <Pane> views = List.of(new Painting1(),new Painting2(),new FillPolygon());
   private final List <Button> buttons= new ArrayList<>();


    public MainMenuView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
      for (Pane view :views){
        String viewName=view.getClass().getSimpleName();
        if (viewName.toLowerCase(Locale.ROOT).endsWith("view"))
          viewName = viewName.substring(0,viewName.length()-"View".length());
        buttons.add(new Button(viewName));
      }

    }

    private void layoutNodes() {
      ObservableList<Node> children =getChildren();
      for(Button button:buttons){
        children.add(button);
        button.setMinWidth(150);
      }

        setSpacing(10.0);
        setMinWidth(200.0);
        setPadding(new Insets(20.0, 0.0, 20.0, 0.0));
        setAlignment(Pos.CENTER);
    }

    private void addAndStyleButton(Button button) {
        getChildren().add(button);
        button.setMinWidth(150.0);
    }


  public List<Button> getButtons() {
      return buttons;
  }

  public List<Pane> getViews() {
      return views;
  }
}
