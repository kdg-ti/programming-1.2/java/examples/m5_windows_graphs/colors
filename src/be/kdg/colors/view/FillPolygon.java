package be.kdg.colors.view;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.ImagePattern;

public class FillPolygon extends BorderPane {
  private Canvas canvas;

  public FillPolygon() {
    this.initialiseNodes();
    this.layoutNodes();
  }
  private void initialiseNodes() {
    canvas = new Canvas(500, 500);
  }
  private void layoutNodes() {
    GraphicsContext gc = canvas.getGraphicsContext2D();
    gc.setFill(new ImagePattern(new Image("newyork.jpg")));
    double xpoints[] = {10, 85, 110, 135, 210, 160,
      170, 110,  50,  60};
    double ypoints[] = {85, 75,  10,  75,  85, 125,
      190, 150, 190, 125};
    gc.fillPolygon(xpoints, ypoints, xpoints.length);
    gc.beginPath();
    gc.moveTo(200,200);
    gc.lineTo(300,200);
    gc.lineTo(300,300);
    gc.lineTo(200,300);
    gc.closePath();
    gc.fill();
    this.setCenter(canvas);
  }
}