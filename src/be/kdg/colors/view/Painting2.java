package be.kdg.colors.view;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;


public class Painting2 extends BorderPane {
    private Canvas canvas;

    public Painting2() {
        this.initialiseNodes();
        this.layoutNodes();
    }
    private void initialiseNodes() {
        canvas = new Canvas(500, 500);
    }
    private void layoutNodes() {
        GraphicsContext gc
          = canvas.getGraphicsContext2D();
        gc.setStroke(Color.web("#2FEEA5"));
        gc.strokeLine(10, 10, 490, 490);
        gc.setStroke(Color.hsb(0.8, 0.3, 0.9));
        gc.strokeOval(100, 100, 50, 50);
        gc.setFill(Color.rgb(30, 230, 80, 0.8));
        gc.fillOval(200, 200, 180, 80);
        gc.setFill(Color.BURLYWOOD);
        gc.fillRect(10, 300, 100, 100);
        this.setCenter(canvas);
    }
}