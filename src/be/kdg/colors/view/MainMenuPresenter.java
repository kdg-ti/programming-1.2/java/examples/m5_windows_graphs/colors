package be.kdg.colors.view;


import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.util.List;

public class MainMenuPresenter {
    private final MainMenuView view;

    public MainMenuPresenter(MainMenuView view) {
        this.view = view;
        // updateView(); This is a static be.kdg.colors.view
        addEventHandlers();
    }

    /*private void updateView() {
    }*/

    private void addEventHandlers() {
        List<Button> buttons = view.getButtons();
        List <Pane> views = view.getViews();
        for (int i = 0;i<buttons.size();i++){
            buttons.get(i).setOnAction(createSimpleEventHandlerForNewWindow(new Scene(views.get(i)), views.get(i).getClass().getSimpleName()));
        }
    }

    private EventHandler<ActionEvent> createSimpleEventHandlerForNewWindow(Scene scene, String windowTitle) {
        return (event) -> {
            Stage stage = new Stage();
            stage.initOwner(view.getScene().getWindow());
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(scene);
            stage.setX(view.getScene().getWindow().getX() + 100);
            stage.setY(view.getScene().getWindow().getY() + 100);
            stage.setTitle(windowTitle);
            stage.showAndWait();
            //stage.sizeToScene();
        };
    }
}
